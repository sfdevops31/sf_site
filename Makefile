obj:
    g++ -c main.C -o main.o

all: obj
    g++ main.o -o helloWorld

clear:
    rm -rf *.o
